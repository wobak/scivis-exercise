#version 150

in vec2 fTexCoord;

uniform sampler3D volume_texture;
uniform ivec3     volume_dimensions;
uniform int       slice;

out vec4 FragColor;

void main() {
    // calculate normaliced fragment coordinates
    vec3 sampling_pos = vec3(fTexCoord.x, fTexCoord.y, (slice + 0.5f) / volume_dimensions.z);
    // calculate 1-pixel offsets
    vec3 x_offset = vec3(1.0f / volume_dimensions.x, 0.0f, 0.0f);
    vec3 y_offset = vec3(0.0f, 1.0f / volume_dimensions.y, 0.0f);
    vec3 z_offset = vec3(0.0f, 0.0f, 1.0f / volume_dimensions.z);

    vec3 gradient = normalize(vec3(
     texture(volume_texture, sampling_pos + x_offset).r - texture(volume_texture, sampling_pos - x_offset).r,
     texture(volume_texture, sampling_pos + y_offset).r - texture(volume_texture, sampling_pos - y_offset).r,
     texture(volume_texture, sampling_pos + z_offset).r - texture(volume_texture, sampling_pos - z_offset).r));

    FragColor = vec4(gradient, texture(volume_texture, sampling_pos).r);
}