#version 330
                                                            
uniform sampler2D transfer_texture;

in vec2 fTexCoord;

uniform float sampling_distance;
uniform float sampling_distance_ref;

uniform int resolution;

layout(location = 0) out vec4 FragColor;

void main()
{
  // coordinates of samples on transfer function
  int front = int(gl_FragCoord.x);
  int back = int(gl_FragCoord.y);
  float step = 1.0f / resolution;
  // sample from s_f to s_b including both points
  int num_samples = abs(front - back) + 1;
  vec4 color_integral = vec4(0.0f);
  float transp_integral = 0;

  int direction = (front <= back) ? 1 : -1; 

  for(int sample = 0; sample < num_samples; ++sample) {
    int omega = front + direction * sample;

    vec4 omega_color = texture(transfer_texture, vec2(omega * step, 0));
    // omega_color.a = 1 - pow(1.0000001 - omega_color.a, sampling_distance / sampling_distance_ref / (num_samples - 1));
    transp_integral += omega_color.a;

    // multiply color contribution with sample transparency
    omega_color.rgb *= omega_color.a;

    float occlusion_intergral = 0.0f;
    // number of samples occluding omega, omega not included
    int num_occluders = abs(front - omega); 
    // go from front to current sample
    for(int sample2 = 0; sample2 < num_occluders; ++sample2) {
      int occluder = front + direction * sample2;

      occlusion_intergral += texture(transfer_texture, vec2(occluder * step, 0)).a;
      if(occlusion_intergral >= 1.0f) break;
    } 
    // divide by number of samples, omega included
    occlusion_intergral /= num_occluders + 1;

    // add obscuring
    omega_color.rgb *= exp( -(num_occluders * sampling_distance / sampling_distance_ref) * occlusion_intergral);
    color_integral.rgb += omega_color.rgb;
  }
  // make values independent from number of samples
  transp_integral /= num_samples;
  color_integral /= num_samples;

  color_integral.a = 1 - exp( -1 * transp_integral);
  // color_integral.a = 1 - exp( -sampling_distance / sampling_distance_ref * transp_integral);
  FragColor= color_integral;
}