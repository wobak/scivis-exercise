#version 330
                                                        
layout(location = 0) in vec3 position;
layout(location = 1) in vec2 vtexcoord; 
out vec2 fTexCoord;

void main()
{
    fTexCoord = vtexcoord;
    gl_Position = vec4(position, 1.0);
}