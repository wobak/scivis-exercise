#version 150
//#extension GL_ARB_shading_language_420pack : require
#extension GL_ARB_explicit_attrib_location : require

#define TASK 21  // 21 22 31 32 33 4 5
#define ENABLE_OPACITY_CORRECTION 0
#define ENABLE_LIGHTNING 0
#define ENABLE_SHADOWING 0

in vec3 ray_entry_position;

layout(location = 0) out vec4 FragColor;

uniform mat4 Modelview;

uniform sampler3D volume_texture;
uniform sampler2D transfer_texture;
uniform sampler2D preintegration_texture;


uniform vec3    camera_location;
uniform float   sampling_distance;
uniform float   sampling_distance_ref;
uniform float   iso_value;
uniform vec3    max_bounds;
uniform ivec3   volume_dimensions;

uniform vec3    light_position;
uniform vec3    light_ambient_color;
uniform vec3    light_diffuse_color;
uniform vec3    light_specular_color;
uniform float   light_ref_coef;

uniform int   refinement_num;
uniform int   classification;

uniform float transfer_peaks[128];

bool
inside_volume_bounds(const in vec3 sampling_position)
{
    return (   all(greaterThanEqual(sampling_position, vec3(0.0)))
            && all(lessThanEqual(sampling_position, max_bounds)));
}
vec3 get_closest_voxel(in vec3 sampling_pos) {
    // get position in pixels
    sampling_pos *= volume_dimensions;
    // get absolute pixel value
    sampling_pos = round(sampling_pos) + vec3(0.5, 0.5, 0.5);
    return sampling_pos / volume_dimensions;
}

float
get_sample_data(vec3 in_sampling_pos){
    vec3 obj_to_tex = vec3(1.0) / max_bounds;
    return texture(volume_texture, in_sampling_pos * obj_to_tex).a;

}

float
get_sample_data3(vec3 in_sampling_pos){
    vec3 obj_to_tex = vec3(1.0) / max_bounds;
    return texture(volume_texture, in_sampling_pos * obj_to_tex).a;

}

vec4
get_preintegrated_sample(vec3 in_sampling_pos1, vec3 in_sampling_pos2){
    return texture(preintegration_texture, vec2(get_sample_data(in_sampling_pos1), get_sample_data(in_sampling_pos2)));
}

float get_sample_data2(in vec3 in_sampling_pos) {

    vec3 blf = (floor(in_sampling_pos * volume_dimensions)) / volume_dimensions;
    vec3 trb = (ceil(in_sampling_pos * volume_dimensions)) / volume_dimensions;
    vec3 brf = vec3(trb.x, blf.y, blf.z);
    vec3 brb = vec3(trb.x, blf.y, trb.z);
    vec3 blb = vec3(blf.x, blf.y, trb.z);

    vec3 tlf = vec3(blf.x, trb.y, blf.z);
    vec3 trf = vec3(trb.x, trb.y, blf.z);
    vec3 tlb = vec3(blf.x, trb.y, trb.z);

    float s_blf = get_sample_data3(blf);
    float s_brf = get_sample_data3(brf);
    float s_brb = get_sample_data3(brb);
    float s_blb = get_sample_data3(blb);
    float s_tlf = get_sample_data3(tlf);
    float s_trf = get_sample_data3(trf);
    float s_trb = get_sample_data3(trb);
    float s_tlb = get_sample_data3(tlb);
    // distance to maximal sample, normalized
    vec3 dist = (trb - in_sampling_pos) * volume_dimensions;

    float result = ((s_blf * dist.x + s_brf * (1 - dist.x)) * dist.z
                + (s_blb * dist.x + s_brb * (1 - dist.x)) * (1 - dist.z)) * dist.y
                + ((s_tlf * dist.x + s_trf * (1 - dist.x)) * dist.z
                + (s_tlb * dist.x + s_trb * (1 - dist.x)) * (1 - dist.z)) * (1 - dist.y);
    return result;
}

vec3 get_gradient(const vec3 sampling_pos) {
    return texture(volume_texture, sampling_pos / max_bounds).rgb;
}

bool is_lit(in vec3 sampling_pos) {
    /// One step trough the volume
    vec3 ray_increment = normalize(sampling_pos - light_position) * sampling_distance;
    /// check if we are inside volume
    bool inside_volume = inside_volume_bounds(sampling_pos);
    // go from sampling pos to light
    while (inside_volume && length(light_position - sampling_pos) > 0.01) {
        // increment the ray sampling position
        sampling_pos += ray_increment;
       
        // check if something solid
        if(get_sample_data(sampling_pos) > iso_value) {
            return false;
        }

        // update the loop termination condition
        inside_volume  = inside_volume_bounds(sampling_pos);
    } 
    return true;
}

vec4 get_phong(const vec3 sampling_pos, float s) {
    // apply the transfer functions to retrieve color and opacity
    vec4 color = texture(transfer_texture, vec2(s, 0));
    // calculate surface normal
    vec3 normal = get_gradient(sampling_pos);
    // calculate vector to light
    vec3 to_light = normalize(light_position - sampling_pos);
    // calculate angle from surface to light
    float angle_to_light = dot(normal, to_light);
    // add ambient term
    vec3 phong = light_ambient_color * color.rgb;

    // check for direct illumination
#if ENABLE_SHADOWING == 1 // Add Shadows
    if(angle_to_light > 0 && is_lit(sampling_pos)) {
#else
    if(angle_to_light > 0) {
#endif
        // add difuse term
        phong += (light_diffuse_color * color.rgb) * angle_to_light;
        
        // calculare reflected light vector
        vec3 ref_light = reflect(-to_light, normal);
        // calculate angle from reflected light to camera
        float angle_to_cam = clamp(dot(ref_light, normalize(sampling_pos - camera_location)), 0, 1);
        // add specular term
        phong += (light_specular_color * vec3(1, 1, 1)) * pow(angle_to_cam, light_ref_coef);
    }

    return vec4(phong, color.w);
}


vec4 get_phong(const vec3 sampling_pos) {
    // get sample
    float s = get_sample_data(sampling_pos);
    return get_phong(sampling_pos, s);
}

void main()
{
    /// One step trough the volume
    vec3 ray_increment      = normalize(ray_entry_position - camera_location) * sampling_distance;
    /// Position in Volume
    vec3 sampling_pos       = ray_entry_position + ray_increment; // test, increment just to be sure we are in the volume

    /// Init color of fragment
    vec4 dst = vec4(0.0, 0.0, 0.0, 0.0);

    /// check if we are inside volume
    bool inside_volume = inside_volume_bounds(sampling_pos);
    
    if (!inside_volume)        
        discard;

#if TASK == 21 // ASSIGNMENT 1
    vec4 max_val = vec4(0.0, 0.0, 0.0, 0.0);
  
    // the traversal loop,
    // termination when the sampling position is outside volume boundarys
    // another termination condition for early ray termination is added
    while (inside_volume) 
    {      
        // get sample
        float s = get_sample_data(sampling_pos);
                
        // apply the transfer functions to retrieve color and opacity
        vec4 color = texture(transfer_texture, vec2(s, s));
           
        // this is the example for maximum intensity projection
        max_val.r = max(color.r, max_val.r);
        max_val.g = max(color.g, max_val.g);
        max_val.b = max(color.b, max_val.b);
        max_val.a = max(color.a, max_val.a);
        
        // increment the ray sampling position
        sampling_pos  += ray_increment;

        // update the loop termination condition
        inside_volume  = inside_volume_bounds(sampling_pos);
    }

    dst = max_val;
#endif 
    
#if TASK == 22 // ASSIGNMENT 1
    vec4 total_val = vec4(0.0, 0.0, 0.0, 0.0);
    int num_samples = 0;
    // the traversal loop,
    // termination when the sampling position is outside volume boundarys
    // another termination condition for early ray termination is added
    while (inside_volume)
    {      
        // get sample
        float s = get_sample_data(sampling_pos);

        // apply the transfer functions to retrieve color and opacity
        vec4 color = texture(transfer_texture, vec2(s, s));
        
        // accumulate total color
        total_val += color;
        
        // increment the ray sampling position
        sampling_pos  += ray_increment;

        // update the loop termination condition
        inside_volume  = inside_volume_bounds(sampling_pos);

        ++num_samples;
    }
    // average total color
    dst = total_val / num_samples;
#endif
    
#if TASK == 31  // ASSIGNMENT 2 & 3 & 4
    // the traversal loop,
    // termination when the sampling position is outside volume boundarys
    // another termination condition for early ray termination is added
    
    // cache value of previous sample
    float previous_s = get_sample_data(sampling_pos); 
    float transparency = 1;

    while (inside_volume)
    {
        // get sample
        float s = get_sample_data(sampling_pos);
        int curr_peak = 0;
        float peak_density = transfer_peaks[0];
        for(int curr_peak = 0; peak_density > 0; peak_density = transfer_peaks[++curr_peak]) {
            // check if cell is inside contour
            if(s >= peak_density && previous_s < peak_density ||
                s < peak_density && previous_s >= peak_density) {

                // binary search for surface
                bool in_surface = true;
                for(float i = 1; i <= refinement_num; ++i) {
                    if(in_surface) {
                        sampling_pos -= ray_increment / exp2(i);
                    }
                    else {
                        sampling_pos += ray_increment / exp2(i);
                    }

                    in_surface = get_sample_data(sampling_pos) >= peak_density;
                }

    #if ENABLE_LIGHTNING == 1 // Add Shading
                vec4 color = get_phong(sampling_pos, peak_density);
    #else
                vec4 color = texture(transfer_texture, vec2(peak_density, 0));
    #endif
                // correct intensity with transparency
                color.rgb *= color.a;
                // accumulate color
                dst += color * transparency;
                
                // update transparency
                transparency *= (1 - color.a);

                // there can only be one isosurface at this sample
                break; 
            }
        }

        // check if voxels behind this pos are obscured
        if (transparency <= 0) break; 

        previous_s = s;

        // increment the ray sampling position
        sampling_pos += ray_increment;

        // update the loop termination condition
        inside_volume = inside_volume_bounds(sampling_pos);
    }
#endif 

#if TASK == 41 || TASK == 42// ASSIGNMENT 5 & 6 & 7
    // the traversal loop,
    // termination when the sampling position is outside volume boundarys
    // another termination condition for early ray termination is added
    float transparency = 1;
    vec3 prev_position = sampling_pos - ray_increment;
// back-to-front
#if TASK == 42
    //find end of volume 
    while (inside_volume)
    {
        // increment the ray sampling position
        sampling_pos += ray_increment;
        // update the loop termination condition
        inside_volume = inside_volume_bounds(sampling_pos);
    }

    // go back in volume
    sampling_pos -= ray_increment;
    inside_volume = true;

#endif

    while (inside_volume)
    {
        // get sample
        float s = get_sample_data(sampling_pos);

#if ENABLE_LIGHTNING == 1 
        // Shade point
        vec4 color = get_phong(sampling_pos);
#else
        vec4 color;
        if(classification == 1){
            // apply the transfer functions to retrieve color and opacity
            color = texture(transfer_texture, vec2(s, s));
        }
        else {
            #if TASK == 42
                //back-to-front, sample order is reversed 
                color = get_preintegrated_sample(sampling_pos, prev_position);
            #else
                color = get_preintegrated_sample(prev_position, sampling_pos);
            #endif
        }
#endif

#if ENABLE_OPACITY_CORRECTION == 1 // Opacity Correction
    if(classification == 1) {
        color.a = 1 - pow(1.0000001 - color.a, sampling_distance / sampling_distance_ref);
    }
#endif

// back-to-front
#if TASK == 42
        // accumulate color
        dst.rgb = color.rgb * color.a + dst.rgb * (1 - color.a);
       
        // accumulate transparency
        dst.a = color.a + dst.a * (1 - color.a);
       
        prev_position = sampling_pos;
        // decrement the ray sampling position
        sampling_pos -= ray_increment;
// front-to-back
#else
        // correct intensity with transparency
        color.rgb *= color.a;
        // accumulate color
        dst += color * transparency;
        
        // update transparency
        transparency *= (1 - color.a);

        // check if voxels behind this pos are obscured
        if (transparency <= 0) break;

        prev_position = sampling_pos;
        // increment the ray sampling position
        sampling_pos += ray_increment;
#endif
        // update the loop termination condition
        inside_volume = inside_volume_bounds(sampling_pos);

    }
#endif 

    // return the calculated color value
    FragColor = dst;
}
